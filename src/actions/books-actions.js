import * as types from './types'

export function getBooksSuccess({books}) {
  return {
    type: types.GET_BOOKS_SUCCESS,
    books
  }
}
