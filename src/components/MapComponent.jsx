import React, { Component } from 'react'
import GoogleMapReact from 'google-map-react'
import icon from './../marker.png'
import './infowindow.css'
const K_WIDTH = 24;
const K_HEIGHT = 38;
const greatPlaceStyle = {
  position: 'absolute',
  width: K_WIDTH,
  height: K_HEIGHT,
  left: -K_WIDTH / 2,
  top: -K_HEIGHT,
  backgroundImage: `url(${icon})`,
  textAlign: 'center',
  color: '#000',
  fontSize: 12,
  fontWeight: 'bold',
}
const Marker = ({ text }) => <div style={greatPlaceStyle}><span>{text}</span></div>
let InfoWindow = ({ state }) => <div className={"infowindow " + (state.visible ? "is-active" : "")}><span>dqwdqwdqwd qwd qwd qwd qwd qwd</span></div>
class MapComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      lat: 0,
      lng: 0,
      visible: false,
      center: {
        lat: 46.7869242,
        lng: 36.7339423
      },
      zoom: 11
    }
  }
  onChildClick = (index, props) => {
    this.setState({
      lat: props.lat,
      lng: props.lng,
      visible: true
    })
    if(this.state.visible === true) {
      this.setState({
        visible: false
      })
    }
  }
  render() {
    return (
      <div style={{ height: '400px', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyB_x1Wrq7BOHCkgSCxc1B4SwWHGn6R3Ai0' }}
          defaultCenter={this.state.center}
          defaultZoom={this.state.zoom}
          onChildClick={this.onChildClick}
        >
          <Marker
            lat={46.7869242}
            lng={36.7339423}
            text="1"
          />
          <InfoWindow 
            lat={this.state.lat}
            lng={this.state.lng}
            state={this.state}/>
        </GoogleMapReact>
      </div>
    )
  }
}

export default MapComponent