import React, {Component} from 'react'
import { connect } from 'react-redux'
class BooksDescription extends Component {
  render() {
    return (
      <div>
        <p>{this.props.location.state.book.title}</p>
        {this.props.location.state.book.authors.map((author, ind) => {
          return <p key={ind}>{author}</p>
        })}
        {this.props.books.books.map((item, i) =>
          <span>{item.title}</span>
        )}
      </div>
    )
  }
}
const mapStateToProps = function(store) {
  return {
    books: store.books
  }
}
export default connect(mapStateToProps)(BooksDescription)