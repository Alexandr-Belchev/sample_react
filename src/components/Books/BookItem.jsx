import React, {Component} from 'react'
import {Link} from 'react-router-dom'

class BooksItem extends Component {
  render() {
    const item = this.props.item
    return (
      <div>
        <div>{this.props.children}</div>
        <Link to={{pathname: '/book/'+ item.id, state: {book: item} }}>{item.title}</Link>
      </div>
    )
  }
}
export default BooksItem