import React, {Component} from 'react'
import BookItem from './BookItem.jsx'
import * as booksAPI from '../../api/books-api'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'

import { connect } from 'react-redux';

import Select from 'react-select'
import './modal.css'
import Modal from 'react-modal'
const SignupSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  lastName: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
});
const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' }
];
class BooksList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      books: [],
      modalIsOpen: false
    }
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }
  openModal() {
    this.setState({modalIsOpen: true})
  }
  closeModal() {
    this.setState({modalIsOpen: false})
  }
  async getData() {
    booksAPI.getBooks().then(() => {
      this.setState({
        books: this.props.books.books
      })
    })
  }
  componentDidMount() {
    this.getData()
  }
  render() {
    return (
      <div>
        <button onClick={this.openModal}>Open Modal</button>
        <Modal
          ariaHideApp={false}
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          contentLabel="Example Modal">
          <Formik
            initialValues={{
              firstName: '',
              lastName: '',
              email: '',
              fruit: ''
            }}
            validationSchema={SignupSchema}
            onSubmit={values => {
              console.log(values)
            }}>
            {({ errors, values, touched, setFieldValue }) => (
              <Form className="form">
                <div>
                  <Select
                    onChange={e => setFieldValue('fruit', e)}
                    value={values.fruit}
                    options={options}
                  />
                  <input className="customname" name="firstName"
                    onInput={e => setFieldValue('firstName', e.target.value)} />
                  {errors.firstName && touched.firstName ? (
                    <div className="error">{errors.firstName}</div>
                  ) : null}
                  </div>
                  <div>
                  <Field name="lastName" />
                  {errors.lastName && touched.lastName ? (
                    <div className="error">{errors.lastName}</div>
                  ) : null}
                  </div>
                  <div>
                  <Field name="email" type="email" />
                  {errors.email && touched.email ? <div className="error">{errors.email}</div> : null}
                </div>
                <button type="submit">Submit</button>
              </Form>
            )}
          </Formik>
          <button onClick={this.closeModal}>Close Modal</button>

        </Modal>
        {this.state.books.map((item, i) =>
          <BookItem item={item} key={i}>
            <span>slot</span>
          </BookItem>
        )}
      </div>
    )
  }
}
const mapStateToProps = function(store) {
  return {
    books: store.books
  };
};
export default connect(mapStateToProps)(BooksList);