import React, {Component} from 'react'
import './paralax-js.css'
class ParalaxJs extends Component {
  componentDidMount() {
    document.addEventListener('scroll', () => {
      let scrolled = document.querySelector('body').getBoundingClientRect().y
      document.querySelector('#parallax-bg-1').style.top = 0+(scrolled*.25)+'px'
      document.querySelector('#parallax-bg-2').style.top = 0+(scrolled*.4)+'px'
      document.querySelector('#parallax-bg-3').style.top = 0+(scrolled*.75)+'px'
    })
  }
  render() {
    return (
      <div className="paralax">
        <div id="parallax-bg-3" className="parallax-bg">
          <div id="bg-3-1"></div>
          <div id="bg-3-2"></div>
          <div id="bg-3-3"></div>
          <div id="bg-3-4"></div>
        </div>
        <div id="parallax-bg-2" className="parallax-bg">
          <div id="bg-2-1"></div>
          <div id="bg-2-2"></div>
          <div id="bg-2-3"></div> 
          <div id="bg-2-4"></div>
          <div id="bg-2-5"></div>
          <div id="bg-2-6"></div>
        </div>
        <div id="parallax-bg-1" className="parallax-bg">
          <div id="bg-1-1"></div>
          <div id="bg-1-2"></div>
          <div id="bg-1-3"></div>
          <div id="bg-1-4"></div>
          <div id="bg-1-5"></div>
        </div>
	
      </div>
    )
  }
}
export default ParalaxJs