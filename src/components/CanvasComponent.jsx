import React, { Component } from 'react';
import { SketchPicker } from 'react-color'
import './eraser.css'
class CanvasComponent extends Component {
  constructor(props) {
    super(props)
    this.canvas = React.createRef()
    this.canvasRef = null
    this.context = null
    this.xInitial = null
    this.yInitial = null
    this.xNext = null
    this.yNext = null
    this.clearItem = false
    this.pixelsArray = []
    this.lock = 0
    this.flag = false
    this.pixelSize = 15
  }
  componentDidMount() {
    this.canvasRef = this.canvas.current
    this.context = this.canvasRef.getContext("2d")
    this.canvasRef.addEventListener("click", this.draw, false)
    this.canvasRef.addEventListener("mousedown", this.draw, false)
    this.generateGrid()
  }
  generateGrid = () => {
    let windowWidth = window.innerWidth
    let counter = 0
    while (counter < windowWidth) {
      this.context.strokeStyle = '#d8d8c9'
      this.context.beginPath()
      this.context.moveTo(counter, 0)
      this.context.lineTo(counter, window.innerHeight)
      this.context.moveTo(0, counter)
      this.context.lineTo(window.innerWidth, counter)
      this.context.stroke()
      counter += 15
    }
  }
  mouseMoveDraw = (e, flag) => {
    this.flag = flag
    if(flag && !e.shiftKey) {
      let pixel = this.getPixelSelected(e)
      if(this.clearItem) {
        this.context.clearRect(pixel['x'], pixel['y'], this.pixelSize, this.pixelSize)
      } else {
        this.context.fillRect(pixel['x'], pixel['y'], this.pixelSize, this.pixelSize)
      }
      this.generateGrid()
    }
    if(flag && e.shiftKey) {
      let pixel = this.getPixelSelected(e)
      if(this.lock === 2) {
        if(this.xNext === this.xInitial) {
          if(this.clearItem) {
            this.context.clearRect(this.xInitial, pixel['y'], this.pixelSize, this.pixelSize)
          } else {
            this.context.fillRect(this.xInitial, pixel['y'], this.pixelSize, this.pixelSize)
          }
        } else {
          if(this.clearItem) {
            this.context.clearRect(pixel['x'], this.yInitial, this.pixelSize, this.pixelSize)
          } else {
            this.context.fillRect(pixel['x'], this.yInitial, this.pixelSize, this.pixelSize)
          }
        }
        this.generateGrid()
      }
      if(this.lock === 1) {
        this.xNext = pixel.x
        this.yNext = pixel.y
        this.lock++
      }
      if(this.lock === 0) {
        if(this.xInitial === null &&
          this.yInitial === null) {
          this.xInitial = pixel.x
          this.yInitial = pixel.y
          this.lock++
        }
      }
    }
  }
  draw = (e) => {
    let pixel = this.getPixelSelected(e)
    if(e.type === 'mousedown') {
      this.canvasRef.onmousemove = (e) => {
        this.mouseMoveDraw(e, true)
      }
      if(this.clearItem) {
        this.context.clearRect(pixel['x'], pixel['y'], this.pixelSize, this.pixelSize)
      } else {
        this.context.fillRect(pixel['x'], pixel['y'], this.pixelSize, this.pixelSize)
      }
      this.generateGrid()
    } 
    if(e.type === 'click') {
      this.canvasRef.onmousemove = (e) => {
        this.mouseMoveDraw(e, false)
      }
      this.xInitial = null
      this.yInitial = null
      this.xNext = null
      this.yNext = null
      this.lock = 0
    }
  }
  getPixelSelected = (e) => {
    let rect = this.canvasRef.getBoundingClientRect(),
      pixel = []
    pixel['x'] = Math.floor((e.clientX - rect.left)/this.pixelSize) * this.pixelSize
    pixel['y'] = Math.floor((e.clientY - rect.top)/this.pixelSize) * this.pixelSize
    this.pixelsArray.push(pixel)
    return pixel
  }
  handleColorChange = (e) => {
    this.clearItem = false
    this.context.fillStyle = e.hex
  }
  chooseGum = () => {
    this.clearItem = true
  }
  getPixels = () => {
    console.log(this.pixelsArray)
    // let pixelsUniq = []
    // this.pixelsArray.forEach(el => {
    //   if(!pixelsUniq.includes(el)) {
    //     pixelsUniq.push(el) 
    //   }
    // })
  }
  render() {
    return (
      <div className="canvas-container">
        <div className="palette">
          <SketchPicker onChangeComplete={ this.handleColorChange }/>
          <span onClick={this.getPixels}>Get array of pixels</span>
          <img className="eraser" src="https://cdn.iconscout.com/icon/free/png-256/eraser-81-444685.png" onClick={this.chooseGum} />
        </div>
        <canvas
          width={window.innerWidth}
          height={window.innerHeight}
          ref={this.canvas} />
      </div>
    );
  }
}

export default CanvasComponent
