import React, { Component } from 'react'
import GoogleMapReact from 'google-map-react'
class MapComponent2 extends Component {

  static defaultProps = {
    center: {
      lat: 46.7869242,
      lng: 36.7339423
    },
    zoom: 11
  }
  onGoogleApiLoaded = ({map, maps}) => {
    this.map = map;
    this.maps = maps;
    this.infowindow = maps.InfoWindow();
    var address = {lat: this.props.center.lat, lng: this.props.center.lng};
    var service = new maps.places.PlacesService(map);
    service.nearbySearch({
      location: address,
      radius: 20000,
      types: ['school']
    }, this.callback);
  }

  callback = (results, status) => {
    for(var i = 0; i < results.length; i++) {
      this.renderMarkers(results[i]);
    }
  }

  renderMarkers = (place) => {
    let marker = new this.maps.Marker({
      position: {lat: parseFloat(place.geometry.location.lat()), lng: parseFloat(place.geometry.location.lng())},
      map: this.map,
      title: 'Hello World!'
    });

    marker[0].addListener('click', () => {
      this.infowindow.setContent('test');
      this.infowindow.open(this.map, marker);
    });
  }

  render(){
    return (
        <div style={{height: '400px'}}>
          <GoogleMapReact
            bootstrapURLKeys={{ 
              key: 'AIzaSyB_x1Wrq7BOHCkgSCxc1B4SwWHGn6R3Ai0',
              libraries: 'places' 
            }}
            onGoogleApiLoaded={this.onGoogleApiLoaded}
            defaultCenter={[this.props.center.lat, this.props.center.lng]}
            defaultZoom={15}
          />
        </div>
      );
  }
}

export default MapComponent2