import api from './index'
import store from '../store'
import { getBooksSuccess } from '../actions/books-actions'

/**
 * Get all books
 */
let newApi = new api('https://reactnd-books-api.udacity.com')
export function getBooks() {
  return newApi.get(`/books`)
    .then(response => {
      store.dispatch(getBooksSuccess(response.data))
      return response.data
    })
}
