import axios from 'axios'
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    `(?:^|; )${name.replace(/([.$?*|{}()[\]\\/+^])/g, '\\$1')}=([^;]*)`
  ))
  return matches ? decodeURIComponent(matches[1]) : undefined
}

export default class Api {
  constructor(base) {
    this.base = base
    return axios.create({
      baseURL: this.base,
      headers: {
      "X-Requested-With": "XMLHttpRequest",
      "X-CSRFToken": getCookie("csrftoken"),
      "Authorization": getCookie("csrftoken")
      }
    })
  }
}