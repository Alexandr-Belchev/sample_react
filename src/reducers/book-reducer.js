import * as types from '../actions/types'
const initialState = {
  books: []
}
const bookReducer = function(state=initialState, action) {
  switch(action.type) {
    case types.GET_BOOKS_SUCCESS:
      return Object.assign({}, state, {books: action.books})
    default:
      return state
  }
}

export default bookReducer
