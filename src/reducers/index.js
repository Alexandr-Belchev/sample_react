import { combineReducers } from 'redux'

import bookReducer from './book-reducer'
let reducers = combineReducers({
  books: bookReducer,
})

export default reducers
