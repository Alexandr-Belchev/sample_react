import React from 'react'
import ReactDOM from 'react-dom'
import router from './router'
import { Provider } from "react-redux";
import * as serviceWorker from './serviceWorker'
import store from './store'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import BooksList from './components/Books/BooksList.jsx'
import notFound from './components/notFound'
import BooksDescription from './components/Books/BooksDescription.jsx'
import MapComponent from './components/MapComponent.jsx'
import MapComponent2 from './components/MapComponent2.jsx'
import CanvasComponent from './components/CanvasComponent.jsx'
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={BooksList}></Route>
        <Route path="/map" component={MapComponent}></Route>
        <Route path="/map2" component={MapComponent2}></Route>
        <Route path="/canvas" component={CanvasComponent}></Route>
        <Route path="/book/:bookId" component={BooksDescription}></Route>
        <Route path="*" component={notFound}></Route>
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister()
